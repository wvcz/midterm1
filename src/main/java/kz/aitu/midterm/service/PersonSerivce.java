package kz.aitu.midterm.service;

import kz.aitu.midterm.entity.Person;
import kz.aitu.midterm.repository.PersonRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
@AllArgsConstructor
public class PersonSerivce {
    private final PersonRepository personRepository;

    public List<Person> getAll(){
        return (List<Person>) personRepository.findAll();
    }
    public Person save(Person person){
        return personRepository.save(person);
    }
    public Person getById(Long id){
        return personRepository.findById(id).orElse(null);
    }
    public void deletePerson(long id){
        personRepository.deleteById(id);
    }
    public Person updatePerson(@RequestBody Person person){
        return personRepository.save(person);

    }

}
