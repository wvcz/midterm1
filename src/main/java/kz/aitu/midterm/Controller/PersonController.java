package kz.aitu.midterm.Controller;


import kz.aitu.midterm.entity.Person;
import kz.aitu.midterm.service.PersonSerivce;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class PersonController {
    private final PersonSerivce personSerivce;
    @GetMapping("http://127.0.0.1:8081/api/v2/users/")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(personSerivce.getAll());
    }
    @PostMapping("http://127.0.0.1:8081/api/v2/users/")
    public ResponseEntity<?> CreatePerson(@RequestBody Person person){
        return ResponseEntity.ok(personSerivce.save(person));
    }
    @GetMapping("http://127.0.0.1:8081/api/v2/users/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") long id){
        return ResponseEntity.ok(personSerivce.getById(id));
    }
    @DeleteMapping(path = "http://127.0.0.1:8081/api/v2/users/{id}")
    public void deletePersonById(@PathVariable long id){
        personSerivce.deletePerson(id);
    }
    @PutMapping(path = "http://127.0.0.1:8081/api/v2/users/")
    public ResponseEntity<?>updatePerson(@RequestBody Person person){
        return ResponseEntity.ok(personSerivce.updatePerson(person));
    }
}
