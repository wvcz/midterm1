package kz.aitu.midterm.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "person")
public class Person {
   @Id
    private long id;
   @Column
    private String firstname;
    private String lastname;
    private String city;
    private String phone;
    private String telegram;


}
