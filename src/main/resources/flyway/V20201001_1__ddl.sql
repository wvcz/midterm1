create table if not exists Person
(
    id        serial not null
        constraint person_pk
            primary key,
    firstname varchar default 255,
    lastname  varchar default 255,
    city      varchar default 255,
    phone     varchar default 255,
    telegram  varchar default 255
);

alter table Person
    owner to postgres;
